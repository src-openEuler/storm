%define debug_package %{nil}
%define _prefix /opt
%define _conf_dir %{_sysconfdir}/storm
%define _log_dir %{_var}/log/storm
%define _data_dir %{_sharedstatedir}/storm
Name:           storm
Version:        1.2.4
Release:        2
Summary:        Apache Storm realtime computation system
License:        Apache-2.0
Url:            http://storm.apache.org
Source0:        http://archive.apache.org/dist/%{name}/apache-%{name}-%{version}/apache-%{name}-%{version}.tar.gz
Source1:        %{name}-nimbus.service
Source2:        %{name}-supervisor.service
Source3:        %{name}-logviewer.service
Source4:        %{name}-ui.service
Source5:        %{name}.logrotate
Source6:        %{name}.conf
Source7:        apache-storm-1.2.4.tar.gzaa
Source8:        apache-storm-1.2.4.tar.gzab
Source9:        apache-storm-1.2.4.tar.gzac
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
%{?systemd_requires}
BuildRequires:  systemd
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
Requires: java-1.8.0-openjdk

%description
Apache Storm is a free and open source distributed realtime computation system.
Storm makes it easy to reliably process unbounded streams of data, doing for
realtime processing what Hadoop did for batch processing.

%prep
cat %{SOURCE7} %{SOURCE8} %{SOURCE9} > %{SOURCE0}
%setup -q -n apache-%{name}-%{version}

%build

%install
mkdir -p $RPM_BUILD_ROOT%{_log_dir}
mkdir -p $RPM_BUILD_ROOT%{_data_dir}
mkdir -p $RPM_BUILD_ROOT%{_conf_dir}
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
mkdir -p $RPM_BUILD_ROOT%{_prefix}/storm/
cp -pr bin $RPM_BUILD_ROOT%{_prefix}/storm/
cp -pr lib $RPM_BUILD_ROOT%{_prefix}/storm/
cp -pr external $RPM_BUILD_ROOT%{_prefix}/storm/
cp -pr log4j2 $RPM_BUILD_ROOT%{_prefix}/storm/
cp -pr public $RPM_BUILD_ROOT%{_prefix}/storm/
cp -pr extlib $RPM_BUILD_ROOT%{_prefix}/storm/
cp -pr extlib-daemon $RPM_BUILD_ROOT%{_prefix}/storm/
install -p -D -m 755 %{S:1} $RPM_BUILD_ROOT%{_unitdir}/
install -p -D -m 755 %{S:2} $RPM_BUILD_ROOT%{_unitdir}/
install -p -D -m 755 %{S:3} $RPM_BUILD_ROOT%{_unitdir}/
install -p -D -m 755 %{S:4} $RPM_BUILD_ROOT%{_unitdir}/
install -p -D -m 644 %{S:5} $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/storm
install -p -D -m 644 %{S:6} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/storm
install -p -D -m 644 conf/storm.yaml $RPM_BUILD_ROOT%{_conf_dir}/
install -p -D -m 644 conf/storm_env.ini $RPM_BUILD_ROOT%{_conf_dir}/

%post
%systemd_post storm-nimbus.service
%systemd_post storm-supervisor.service
%systemd_post storm-logviewer.service
%systemd_post storm-ui.service

%preun
for module in nimbus supervisor logviewer ui; do
systemctl status storm-$module > /dev/null
if [[ $? -eq 0 ]]; then
%systemd_preun storm-$module.service
fi
done

%files
%defattr(-,root,root)
%{_unitdir}/storm-nimbus.service
%{_unitdir}/storm-supervisor.service
%{_unitdir}/storm-logviewer.service
%{_unitdir}/storm-ui.service
%config(noreplace) %{_sysconfdir}/logrotate.d/storm
%config(noreplace) %{_sysconfdir}/sysconfig/storm
%config(noreplace) %{_conf_dir}/*
%{_prefix}/storm
%dir %{_log_dir}
%dir %{_data_dir}

%changelog
* Wed Sep 28 2022 xiexing <xiexing4@hisilicon.com> - 1.2.4-2
- fix stop service return 143

* Wed Sep 01 2021 wangkai <wangkai385@huawei.com> - 1.2.4-1
- Upgrade to 1.2.4 for fix CVE-2021-38294-CVE-2021-40865

* Fri Aug 20 2021 sunguoshuai <sunguoshuai@huawei.com> - 1.2.3-4
- Fix runtime error and uninstall warning

* Mon Jun 14 2021 lingsheng <lingsheng@huawei.com> - 1.2.3-3
- Fix status failure after stopping service

* Wed May 26 2021 wangchong <wangchong56@huawei.com> - 1.2.3-2
- add requires

* Thu Mar 18 2021 huanghaitao <huanghaitao8@huawei.com> - 1.2.3-1
- Package init
