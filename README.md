# storm

#### 介绍
Apache Storm是一个免费的开源分布式实时计算系统。 Storm可以轻松地处理无限数据流， 并且实时处理Hadoop批处理任务。 可以和任何编程语言一起使用，并且使用起来非常有趣！
Storm有许多用例：实时分析，在线机器学习，连续计算，分布式RPC，ETL等等。 Storm是快速 的：一个基准计算每个节点每秒处理超过一百万个元组。 它具有可扩展性，容错性，可确保您的 数据得到处理，且易于设置和操作。
Storm集成了您已经使用的队列和数据库技术。 Storm拓扑消耗数据流，并以任意复杂的方式处理 这些流，然后重新分配计算的每个阶段之间的流

#### 软件架构
软件架构说明
与Hadoop主从架构一样,Storm也采用Master/Slave体系结构,分布式计算由Nimbus和Supervisor两类服务进程实现,Nimbus进程运行在集群的主节点,负责任务的指派和分发,Supervisor运行在集群的从节点,负责执行任务的具体部分。
- Nimbus： Storm集群的Master节点，负责资源分配和任务调度，负责分发用户代码，指派给具体的Supervisor节点上的Worker节点，去运行Topology对应的组件（Spout/Bolt）的Task。
- Supervisor： Storm集群的从节点，负责接受Nimbus分配的任务，启动和停止属于自己管理的worker进程。通过Storm的配置文件中的supervisor.slots.ports配置项，可以指定在一个Supervisor上最大允许多少个Slot，每个Slot通过端口号来唯一标识，一个端口号对应一个Worker进程（如果该Worker进程被启动）。
- Worker： 负责运行具体处理组件逻辑的进程。Worker运行的任务类型只有两种，一种是Spout任务，一种是Bolt任务。
- Task： worker中每一个spout/bolt的线程称为一个task｡同一个spout/bolt的task可能会共享一个物理线程,该线程称为executor。
- ZooKeeper： 用来协调Nimbus和Supervisor，如果Supervisor因故障出现问题而无法运行Topology，Nimbus会第一时间感知到，并重新分配Topology到其它可用的Supervisor上运行

#### ARM支持：

1. 移植指南：xxxx
2. 部署指南：https://support.huaweicloud.com/dpmg-apache-kunpengbds/kunpengstorm_04_0001.html
3. 调优指南：https://support.huaweicloud.com/tngg-kunpengbds/kunpengstormhdp_05_0002.html

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
